package com.mfs.client.im.dummy.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mfs.client.im.dto.AuthRequestDto;
import com.mfs.client.im.dto.AuthResponseDto;
import com.mfs.client.im.dto.CreatedAt;
import com.mfs.client.im.dto.PayoutResponseDto;
import com.mfs.client.im.dto.Response;
import com.mfs.client.im.dto.TransactionDetailsResponse;
import com.mfs.client.im.dto.Data;
import com.mfs.client.im.dto.ValidateMobileResponseDto;

@RestController
public class InstntDummyController {

	@RequestMapping("/oauth/token")
	public AuthResponseDto authDummyCntrl(@RequestBody AuthRequestDto request) {
		AuthResponseDto response= new AuthResponseDto();
      
		response.setToken_type("Bearer");
		response.setExpires_in("61536000");
		response.setRefresh_token("kkNeAiOiJKV1Jh");
		response.setAccess_token("kkMNehklf");
		
		return response;
	}

	@RequestMapping(value="/api/payout",method=RequestMethod.POST)
	public PayoutResponseDto payoutDummyCntrl(@RequestParam("demo") MultipartFile[] multipart) {

		try {
		String jsonStr="";
		int pointer=0;
		byte b[]=multipart[0].getBytes();
		while(pointer<b.length) {			
			jsonStr+=(char)b[pointer];
			pointer++;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		PayoutResponseDto res = new PayoutResponseDto();
		res.setCode("C200");
		res.setAmount(400.0);
		res.setZeepay_id("Trxn94");
		res.setMessage("Payout Success");

		return res;

	}

	@RequestMapping(value="/payouts/account-verificationt",method=RequestMethod.POST)
	public ValidateMobileResponseDto validateDummyCntrl(@RequestParam("demo") MultipartFile[] multipart) {

		
		
		try {
			String jsonStr="";
			int pointer=0;
			byte b[]=multipart[0].getBytes();
			while(pointer<b.length) {			
				jsonStr+=(char)b[pointer];
				pointer++;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		ValidateMobileResponseDto res = new ValidateMobileResponseDto();
		Response resp = new Response();

		res.setCode("200");
		resp.setRegistered_name("Arjun Shetty");
		resp.setRegistration_status("Registered");
		res.setReponse(resp);

		return res;
	}

	@RequestMapping(value = "/api/transactions/{id}", method = RequestMethod.GET)
	public TransactionDetailsResponse transDetailsDummyCntrl(@PathVariable String id) {
		Data res = new Data();
		TransactionDetailsResponse response=new TransactionDetailsResponse();
		CreatedAt resp = new CreatedAt();

		res.setZeepay_id("Trxn200");
		res.setExtr_id("pdtrans002");
		res.setSender_first_name("pkaran");
		res.setSender_last_name("Gjoshi");
		res.setSender_country("Gindia");
		res.setRecipient_first_name("gAbhi");
		res.setRecipient_last_name("Gpathak");
		res.setService_type("GBank");
		res.setMobile_account("G8291829899");
		res.setFess("fess");
		res.setStatus("success");
		res.setAmount_sent(100000.0);
		res.setAmount_payout(100000.0);

		resp.setDate("2019-17-04 12:30:55.0000");
		resp.setTimezone_type("3");
		resp.setTimezone("UTC");
		res.setCreated_at(resp);
		response.setData(res);
		

		return response;

	}

}
