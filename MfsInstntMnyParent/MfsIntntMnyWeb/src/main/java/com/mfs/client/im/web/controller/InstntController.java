package com.mfs.client.im.web.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.im.dto.AuthResponseDto;
import com.mfs.client.im.dto.PayoutRequestDto;
import com.mfs.client.im.dto.PayoutResponseDto;
import com.mfs.client.im.dto.ResponseStatus;
import com.mfs.client.im.dto.TransactionDetailsResponse;
import com.mfs.client.im.dto.Data;
import com.mfs.client.im.dto.ValidateMobileRequestDto;
import com.mfs.client.im.dto.ValidateMobileResponseDto;
import com.mfs.client.im.service.AuthService;
import com.mfs.client.im.service.GetTransactionService;
import com.mfs.client.im.service.PayoutService;
import com.mfs.client.im.service.ValidateService;
import com.mfs.client.im.service.ValidateService1;
import com.mfs.client.im.util.CommonConstant;
import com.mfs.client.im.util.CommonValidations;
import com.mfs.client.im.util.MFSIMCode;

@RestController
public class InstntController {

	@Autowired
	AuthService authService;

	@Autowired
	PayoutService payoutService;

	@Autowired
	ValidateService1 validateService;

	@Autowired
	GetTransactionService getTransactionService;

	private static final Logger LOGGER = Logger.getLogger(InstntController.class);

	@RequestMapping("/auth")
	@ResponseBody
	public AuthResponseDto authCntrl() {

		LOGGER.info("Inside InstntController");

		AuthResponseDto response = null;

		response = authService.getAccessToken();

		if (response == null) {
			response = new AuthResponseDto();
			response.setCode(MFSIMCode.IMCODE_ERROR_TIMEOUT.getCode());
			response.setMessage(MFSIMCode.IMCODE_ERROR_TIMEOUT.getMessage());
		}
		LOGGER.info("Inside InstntController --->Auth Response" + response);
		return response;

	}

	@RequestMapping("/payout")
	@ResponseBody
	public PayoutResponseDto payoutCntrl(@RequestBody PayoutRequestDto request) {

		LOGGER.info("Inside InstntController in payoutCntrl function" + request);
		CommonValidations validations = new CommonValidations();
		PayoutResponseDto response = null;
		if (request != null) {
			if (validations.validateStringValues(request.getSender_first_name())) {
				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SENDER_FIRST_NAME);
			} else if (validations.validateStringValues(request.getSender_last_name())) {
				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SENDER_LAST_NAME);
			} else if (validations.validateStringValues(request.getSender_country())) {
				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SENDER_COUNTRY);
			} else if (validations.validateStringValues(request.getReceiver_first_name())) {
				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_FIRST_NAME);
			} else if (validations.validateStringValues(request.getReceiver_last_name())) {
				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_LAST_NAME);
			} else if (validations.validateStringValues(request.getReceiver_country())) {
				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_COUNTRY);
			} else if (validations.validateStringValues(request.getReceiver_currency())) {
				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_CURRENCY);
			} else if (validations.validateAmountValues(request.getAmount()) || request.getAmount() == 0.0
					|| request.getAmount() == 0) {
				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_AMOUNT);
			} else if (validations.validateStringValues(request.getExtr_id())) {
				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_EXTR_ID);
			} else if (validations.validateStringValues(request.getService_type())
					&& (!request.getService_type().equalsIgnoreCase("bank"))
					&& (!request.getService_type().equalsIgnoreCase("wallet"))
					&& (!request.getService_type().equalsIgnoreCase("pickup"))) {
				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SERVICE_TYPE);
			} else if (request.getService_type().equalsIgnoreCase("bank")
					&& validations.validateStringValues(request.getAccount_number())) {

				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_ACCOUNT_NUMBER);
			} else if (request.getService_type().equalsIgnoreCase("bank")
					&& validations.validateStringValues(request.getRouting_number())) {
				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_ROUTING_NUMBER);

			} else if ((request.getService_type().equalsIgnoreCase("wallet")
					|| request.getService_type().equalsIgnoreCase("pickup"))
					&& validations.validateStringValues(request.getMno())) {

				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_MNO);
			} else if ((request.getService_type().equalsIgnoreCase("wallet")
					|| request.getService_type().equalsIgnoreCase("pickup"))
					&& validations.validateStringValues(request.getReceiver_msisdn())) {

				response = new PayoutResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_MSISDN);
			} else {
				response = payoutService.payout(request);
				if (response == null) {
					response = new PayoutResponseDto();
					ResponseStatus responseStatus = new ResponseStatus();
					responseStatus.setStatusCode(MFSIMCode.ER203.getCode());
					responseStatus.setStatusMessage(MFSIMCode.ER203.getMessage());
					response.setResponseStatus(responseStatus);
				}

			}
		} else {
			response = new PayoutResponseDto();
			ResponseStatus responseStatus = new ResponseStatus();
			responseStatus.setStatusCode(MFSIMCode.ER203.getCode());
			responseStatus.setStatusMessage(MFSIMCode.ER203.getMessage());
			response.setResponseStatus(responseStatus);
		}
		LOGGER.info("Inside InstntController --->Payout Response" + response);
		return response;

	}

	@RequestMapping("/validateMobileWallet")
	@ResponseBody
	public ValidateMobileResponseDto validateMobileWalletCntrl(@RequestBody ValidateMobileRequestDto request) {

		LOGGER.info("Inside InstntController in ValidateMobile function" + request);
		ValidateMobileResponseDto response = null;
		CommonValidations validation = new CommonValidations();

		if (request != null) {
			if (validation.validateStringValues(request.getService_type())) {

				response = new ValidateMobileResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SERVICE_TYPE);

			} else if (request.getService_type().equalsIgnoreCase("bank")
					&& validation.validateStringValues(request.getRouting_number())) {
				response = new ValidateMobileResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_ROUTING_NUMBER);

			} else if (request.getService_type().equalsIgnoreCase("bank")
					&& validation.validateStringValues(request.getAccount_number())) {

				response = new ValidateMobileResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_ACCOUNT_NUMBER);

			} else if (request.getService_type().equalsIgnoreCase("wallet")
					&& validation.validateStringValues(request.getMobile_number())) {
				response = new ValidateMobileResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(
						MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_RECEIVER_MSISDN);
			} else if (request.getService_type().equalsIgnoreCase("wallet")
					&& validation.validateStringValues(request.getMno())) {

				response = new ValidateMobileResponseDto();
				response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
				response.setMessage(MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_MNO);

			} else {
				response = validateService.validate(request);
				if (response == null) {
					response = new ValidateMobileResponseDto();
					response.setCode(MFSIMCode.ER203.getCode());
					response.setMessage(MFSIMCode.ER203.getMessage());
				}
			}
		}
		LOGGER.info("Inside InstntController --->ValidateMobile Response" + response);
		return response;
	}

	@GetMapping("/getTransactionStatus")
	@ResponseBody
	public TransactionDetailsResponse getTransactionStatusCntrl(@RequestParam String id) {
		LOGGER.info("Inside InstntController --->TransactionDetails Request" + id);
		TransactionDetailsResponse response = null;
		Data data = null;

		if (id == null || id.equalsIgnoreCase("null")) {
			data = new Data();
			response = new TransactionDetailsResponse();
			data.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
			data.setMessage(MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_ID);
			response.setData(data);

		} else {

			response = getTransactionService.getTransactionDetails(id);

			if (response == null) {
				data = new Data();
				response = new TransactionDetailsResponse();
				data.setCode(MFSIMCode.ER203.getCode());
				data.setMessage(MFSIMCode.ER203.getMessage());
				response.setData(data);

			}
		}
		LOGGER.info("Inside InstntController --->TransactionDetails Response" + response);
		return response;
	}

}
