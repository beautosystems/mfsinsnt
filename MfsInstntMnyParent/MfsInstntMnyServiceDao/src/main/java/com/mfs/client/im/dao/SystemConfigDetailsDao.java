package com.mfs.client.im.dao;

import java.util.Map;

import com.mfs.client.im.exception.DaoException;

public interface SystemConfigDetailsDao {
	
	public Map<String,String> getConfigDetailsMap()throws DaoException;

}
