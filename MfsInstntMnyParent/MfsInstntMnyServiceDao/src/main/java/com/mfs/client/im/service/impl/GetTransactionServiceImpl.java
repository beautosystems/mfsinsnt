package com.mfs.client.im.service.impl;

import java.text.ParseException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.im.dao.SystemConfigDetailsDao;
import com.mfs.client.im.dao.TransactionDao;
import com.mfs.client.im.dto.Data;
import com.mfs.client.im.dto.TransactionDetailsResponse;
import com.mfs.client.im.exception.DaoException;
import com.mfs.client.im.model.ImTransactionLog;
import com.mfs.client.im.service.GetTransactionService;
import com.mfs.client.im.util.CallServices;
import com.mfs.client.im.util.CommonConstant;
import com.mfs.client.im.util.MFSIMCode;
import com.mfs.client.util.JSONToObjectConversion;

@Service("GetTransactionService")
public class GetTransactionServiceImpl extends BaseServiceImpl implements GetTransactionService {

	private static final Logger LOGGER = Logger.getLogger(ValidateServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;
	@Autowired
	SystemConfigDetailsDao systemconfigdao;

	public TransactionDetailsResponse getTransactionDetails(String id) {

		LOGGER.info("==>In  GetTransactionServiceImpl in T function gettransactionRequest =" + id);
	
		String getTansResponse = "";
		TransactionDetailsResponse response = null;
		Data data=null;

		try {
		Map<String, String> systemconfigdetails = systemconfigdao.getConfigDetailsMap();
		
		
			LOGGER.info("==>In GetTransactionServiceImpl Sending Request to Url : "
					+ systemconfigdetails.get(CommonConstant.BASE_URL)
					+ systemconfigdetails.get(CommonConstant.STATUS_TRANS_URL));

			
			getTansResponse = CallServices.getTransactionResponseFromService(
					systemconfigdetails.get(CommonConstant.BASE_URL)
							+ systemconfigdetails.get(CommonConstant.STATUS_TRANS_URL),id,
					systemconfigdetails.get(CommonConstant.ACCESS_TOKEN));
			
			LOGGER.info("==>In GettransactionServiceImpl Response from HTTP Method : " + getTansResponse);

			
			response = (TransactionDetailsResponse) JSONToObjectConversion.getObjectFromJson(getTansResponse,
					TransactionDetailsResponse.class);
			
			
			LOGGER.info("==>In validateServiceImpl String Response from HTTP method :" + response.toString());

			if (response != null) {
				
				updateTransactionDetails(response, id);
			}

		} catch (DaoException de) {
			LOGGER.error("==>DaoException in GetTransactionServiceImpl" + de);
			response = new TransactionDetailsResponse();
			data=new Data();
			
			data.setCode(de.getStatus().getStatusCode());
			data.setMessage(de.getStatus().getStatusMessage());
			response.setData(data);

		} catch (Exception e) {

			LOGGER.error("Exception in GetTransactionServiceImpl " + e);
			data = new Data();
			response = new TransactionDetailsResponse();
			data.setMessage(MFSIMCode.ER203.getMessage());
			data.setCode(MFSIMCode.ER203.getCode());
			response.setData(data);

		}

		return response;
	}

	public void updateTransactionDetails(TransactionDetailsResponse resp, String id)
			throws ParseException, DaoException {

		
		ImTransactionLog logModel = transactionDao.getTransactionByThirdPartyTransId(id);

		
		if (logModel != null) {
			
		

			logModel.setThirdPartyTransId(resp.getData().getZeepay_id());
			logModel.setMfsTransId(resp.getData().getExtr_id());
			logModel.setSenderFirstName(resp.getData().getSender_first_name());
			logModel.setSenderLastName(resp.getData().getSender_last_name());
			logModel.setSenderCountry(resp.getData().getSender_country());
			logModel.setReceiverFirstName(resp.getData().getRecipient_first_name());
			logModel.setReceiverLastName(resp.getData().getRecipient_last_name());
			logModel.setServiceType(resp.getData().getService_type());
			logModel.setAccountNumber(resp.getData().getMobile_account());
			logModel.setStatus(resp.getData().getStatus());
			logModel.setAmountSent(resp.getData().getAmount_sent());
			logModel.setAmountPayout(resp.getData().getAmount_payout());
			logModel.setDateLogged(resp.getData().getCreated_at().getDate());
			
			
			logModel.setMessage(CommonConstant.UPDATE_STATUS);
			logModel.setFess(resp.getData().getFess());
			logModel.setTimezone(resp.getData().getCreated_at().getTimezone());
			logModel.setTimezoneType(resp.getData().getCreated_at().getTimezone_type());

			transactionDao.update(logModel);
		}

	}

}