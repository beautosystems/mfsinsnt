package com.mfs.client.im.service.impl;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mfs.client.im.dao.SystemConfigDetailsDao;
import com.mfs.client.im.dao.TransactionDao;
import com.mfs.client.im.dto.ValidateMobileRequestDto;
import com.mfs.client.im.dto.ValidateMobileResponseDto;
import com.mfs.client.im.exception.DaoException;
import com.mfs.client.im.model.ImKycDetails;
import com.mfs.client.im.service.ValidateService;
import com.mfs.client.im.util.CallServices;
import com.mfs.client.im.util.CommonConstant;
import com.mfs.client.im.util.MFSIMCode;

@Service("ValidateService")
public class ValidateServiceImpl extends BaseServiceImpl implements ValidateService {

	private static final Logger LOGGER = Logger.getLogger(ValidateServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;
	@Autowired
	SystemConfigDetailsDao systemconfigdao;

	public ValidateMobileResponseDto validate(ValidateMobileRequestDto request) {

		LOGGER.info("==>In ValidateServiceImpl in validatemobileWallet function validatemobileRequest  =" + request);
		HttpResponse serviceResponse;
		ValidateMobileResponseDto response = null;
		Gson gson = new Gson();

		if (request.getService_type().equalsIgnoreCase("bank") || request.getService_type().equalsIgnoreCase("wallet")
				|| request.getService_type().equalsIgnoreCase("pickup")) {

			try {
				Map<String, String> systemconfigdetails = systemconfigdao.getConfigDetailsMap();

				Object checkAccount = transactionDao.getValidateMobile(request);

				LOGGER.info("==> In validateServiceImpl Validate Request : " + gson.toJson(request));

				Object obj = gson.toJson(request);

				/*try (FileWriter file = new FileWriter(systemconfigdetails.get(CommonConstant.FILE_PATH))) {
					file.write(obj.toString());

				}*/

				if (checkAccount == null) {

					logValidateMobile(request); // save
					LOGGER.info("==>In validateServiceImpl Sending Request to Url : "
							+ systemconfigdetails.get(CommonConstant.BASE_URL)
							+ systemconfigdetails.get(CommonConstant.VALIDATE_URL));

					serviceResponse = CallServices.getResponseFromServices(
							systemconfigdetails.get(CommonConstant.BASE_URL)
									+ systemconfigdetails.get(CommonConstant.VALIDATE_URL),
							systemconfigdetails.get(CommonConstant.ACCESS_TOKEN),
							systemconfigdetails.get(CommonConstant.FILE_PATH));

					StringBuilder result = new StringBuilder();

					if (serviceResponse.getStatusLine().getStatusCode() == 200) {
						BufferedReader rd = new BufferedReader(
								new InputStreamReader(serviceResponse.getEntity().getContent()));
						String line;
						while ((line = rd.readLine()) != null) {
							result.append(line);
						}

					} else {
						throw new Exception("Error Occurred while pulling transaction");
					}

					ObjectMapper mapper = new ObjectMapper();

					response = mapper.readValue(result.toString(), ValidateMobileResponseDto.class);

					updateValidateMobile(response, request);

					LOGGER.info("==>In validateServiceImpl Response from HTTP Method : " + serviceResponse);
					LOGGER.info("==>In validateServiceImpl String Response from HTTP method :" + response.toString());

				} else {

					LOGGER.info("==>In validateServiceImpl Sending Request to Url : "
							+ systemconfigdetails.get(CommonConstant.BASE_URL)
							+ systemconfigdetails.get(CommonConstant.VALIDATE_URL));

					serviceResponse = CallServices.getResponseFromServices(
							systemconfigdetails.get(CommonConstant.BASE_URL)
									+ systemconfigdetails.get(CommonConstant.VALIDATE_URL),
							systemconfigdetails.get(CommonConstant.ACCESS_TOKEN),
							systemconfigdetails.get(CommonConstant.FILE_PATH));
					LOGGER.info("==>In validateServiceImpl Response from HTTP Method : " + serviceResponse);

					StringBuilder result = new StringBuilder();

					if (serviceResponse.getStatusLine().getStatusCode() == 200) {
						BufferedReader rd = new BufferedReader(
								new InputStreamReader(serviceResponse.getEntity().getContent()));
						String line;
						while ((line = rd.readLine()) != null) {
							result.append(line);
						}

					} else {
						throw new Exception("Error Occurred while validating account");
					}

					ObjectMapper mapper = new ObjectMapper();

					response = mapper.readValue(result.toString(), ValidateMobileResponseDto.class);

					updateValidateMobile(response, request);

					LOGGER.info("==>In validateServiceImpl String Response from HTTP method :" + response.toString());
				}

			} catch (DaoException de) {
				LOGGER.error("==>DaoException in validatemobileWallet" + de);
				response = new ValidateMobileResponseDto();

				response.setCode(de.getStatus().getStatusCode());
				response.setMessage(de.getStatus().getStatusMessage());

			} catch (Exception e) {
				LOGGER.error("==>Exception in validateServiceImpl " + e);

				response = new ValidateMobileResponseDto();
				response.setMessage(MFSIMCode.ER203.getMessage());
				response.setCode(MFSIMCode.ER203.getCode());

			}
		} else {

			response = new ValidateMobileResponseDto();
			response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SERVICE_TYPE);
		}

		return response;
	}

	public void logValidateMobile(ValidateMobileRequestDto request) throws DaoException {

		ImKycDetails logModel = new ImKycDetails();

		logModel.setServiceType(request.getService_type());
		logModel.setRoutingNumber(request.getRouting_number());
		logModel.setAccountNumber(request.getAccount_number());
		logModel.setMobileNumber(request.getMobile_number());
		logModel.setMno(request.getMno());

		transactionDao.save(logModel);

	}

	public void updateValidateMobile(ValidateMobileResponseDto resp, ValidateMobileRequestDto request)
			throws DaoException {

		ImKycDetails kycModel = (ImKycDetails) transactionDao.getUpdateValidateMobile(request);

		if (kycModel != null) {
			if (resp == null) {

				kycModel.setRegistrationStatus(MFSIMCode.ER212.getMessage());
				transactionDao.update(kycModel);
			} else {
				kycModel.setRegisteredName(resp.getReponse().getRegistered_name());
				kycModel.setRegistrationStatus(resp.getReponse().getRegistration_status());

				transactionDao.update(kycModel);
			}
		}

	}

}