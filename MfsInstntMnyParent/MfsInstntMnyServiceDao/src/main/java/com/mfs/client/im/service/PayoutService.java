package com.mfs.client.im.service;

import com.mfs.client.im.dto.PayoutRequestDto;
import com.mfs.client.im.dto.PayoutResponseDto;

public interface PayoutService {
	
	public PayoutResponseDto payout(PayoutRequestDto request);

}
