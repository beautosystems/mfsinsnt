package com.mfs.client.im.service.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.im.dao.SystemConfigDetailsDao;
import com.mfs.client.im.dao.TransactionDao;
import com.mfs.client.im.dto.ValidateMobileRequestDto;
import com.mfs.client.im.dto.ValidateMobileResponseDto;
import com.mfs.client.im.exception.DaoException;
import com.mfs.client.im.model.ImKycDetails;
import com.mfs.client.im.service.ValidateService1;
import com.mfs.client.im.util.CallServices;
import com.mfs.client.im.util.CommonConstant;
import com.mfs.client.im.util.MFSIMCode;
import com.mfs.client.util.JSONToObjectConversion;

@Service("ValidateServiceTest")
public class ValidateServiceImpl1 extends BaseServiceImpl implements ValidateService1  {
	private static final Logger LOGGER = Logger.getLogger(ValidateServiceImpl1.class);

	@Autowired
	TransactionDao transactionDao;
	@Autowired
	SystemConfigDetailsDao systemconfigdao;

	public ValidateMobileResponseDto validate(ValidateMobileRequestDto request) {

		LOGGER.info("==>In ValidateServiceImpl in validatemobileWallet function validatemobileRequest  ="
				+ request);
		String validateResponse = "";
		ValidateMobileResponseDto response = null;
		Gson gson = new Gson();
		
		Map<String, String> systemconfigdetails = null;
		try {
			systemconfigdetails = systemconfigdao.getConfigDetailsMap();
		} catch (DaoException e1) {

		}
		
	

		if (request.getService_type().equalsIgnoreCase("Bank")
				|| request.getService_type().equalsIgnoreCase("Wallet")
				|| request.getService_type().equalsIgnoreCase("PickUp")) {

			
				try {

				Object checkAccount = transactionDao.getValidateMobile(request);

				LOGGER.info("==> In validateServiceImpl Validate Request : "
						+ gson.toJson(request));

				if (checkAccount == null) {

					logValidateMobile(request); // save
					LOGGER.info("==>In validateServiceImpl Sending Request to Url : "+ systemconfigdetails.get(CommonConstant.BASE_URL)
							+ systemconfigdetails.get(CommonConstant.VALIDATE_URL));
					
					validateResponse = CallServices.getResponseFromServices1(systemconfigdetails.get(CommonConstant.BASE_URL)
									+ systemconfigdetails.get(CommonConstant.VALIDATE_URL),
							gson.toJson(request), systemconfigdetails.get(CommonConstant.ACCESS_TOKEN));
					
					

					LOGGER.info("In AuthServiceImpl Response from HTTP Method : "+ validateResponse);
					
					response = (ValidateMobileResponseDto) JSONToObjectConversion.getObjectFromJson(validateResponse,
									ValidateMobileResponseDto.class);

					updateValidateMobile(response, request);

					LOGGER.info("==>In validateServiceImpl Response from HTTP Method : "+ response);
					LOGGER.info("==>In validateServiceImpl String Response from HTTP method :"+ response.toString());

				} else {

					LOGGER.info("==>In validateServiceImpl Sending Request to Url : "+ systemconfigdetails.get(CommonConstant.BASE_URL)
							+ systemconfigdetails.get(CommonConstant.VALIDATE_URL));
					System.out.println(gson.toJson(request));
					validateResponse = CallServices.getResponseFromServices1(systemconfigdetails.get(CommonConstant.BASE_URL)
									+ systemconfigdetails.get(CommonConstant.VALIDATE_URL),
							gson.toJson(request), systemconfigdetails.get(CommonConstant.ACCESS_TOKEN));
					
					
					LOGGER.info("In AuthServiceImpl Response from HTTP Method : "+ validateResponse);
					
					response = (ValidateMobileResponseDto) JSONToObjectConversion.getObjectFromJson(validateResponse,
									ValidateMobileResponseDto.class);

					updateValidateMobile(response, request);

					LOGGER.info("==>In validateServiceImpl Response from HTTP Method : "+ response);
					LOGGER.info("==>In validateServiceImpl String Response from HTTP method :"+ response.toString());

				}
			} catch (DaoException de) {
				LOGGER.error("==>DaoException in validatemobileWallet" + de);
				response = new ValidateMobileResponseDto();

				response.setCode(de.getStatus().getStatusCode());
				response.setMessage(de.getStatus().getStatusMessage());

			} catch (Exception e) {
				LOGGER.error("==>Exception in validateServiceImpl " + e);

				response = new ValidateMobileResponseDto();
				response.setMessage(MFSIMCode.ER203.getMessage());
				response.setCode(MFSIMCode.ER203.getCode());

			}
		} else {

			response = new ValidateMobileResponseDto();
			response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
			response.setMessage(MFSIMCode.VALIDATION_ERROR.getMessage() + " "
					+ CommonConstant.INVALID_SERVICE_TYPE);
		}

		return response;
	}

	public void logValidateMobile(ValidateMobileRequestDto request)
			throws DaoException {

		ImKycDetails logModel = new ImKycDetails();

		logModel.setServiceType(request.getService_type());
		logModel.setRoutingNumber(request.getRouting_number());
		logModel.setAccountNumber(request.getAccount_number());
		logModel.setMobileNumber(request.getMobile_number());
		logModel.setMno(request.getMno());

		transactionDao.save(logModel);

	}

	public void updateValidateMobile(ValidateMobileResponseDto resp,
			ValidateMobileRequestDto request) throws DaoException {

		ImKycDetails kycModel = (ImKycDetails) transactionDao
				.getUpdateValidateMobile(request);

		if (kycModel != null) {
			if (resp == null) {

				kycModel.setRegistrationStatus(MFSIMCode.ER212.getMessage());
				transactionDao.update(kycModel);
			} else {
				kycModel.setRegisteredName(resp.getReponse()
						.getRegistered_name());
				kycModel.setRegistrationStatus(resp.getReponse()
						.getRegistration_status());

				transactionDao.update(kycModel);
			}
		}

	}

}