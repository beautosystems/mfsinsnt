package com.mfs.client.im.service;

import java.util.Date;

public interface BaseService {

	
	public void logTransSession(String senderFirstName,String senderLastName,String receiverFirstName,String receiverLastName,String receiverMsisdn,String mno
			,String mfsTransId,String serviceType,String routingNumber,String accountNumber,String thirdPartyTransId,String message,
			String status,double amountSent,double amountPayout,Date dateLogged);
	
	

	
}
