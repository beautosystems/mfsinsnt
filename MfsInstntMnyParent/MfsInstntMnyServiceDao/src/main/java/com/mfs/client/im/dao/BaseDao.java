package com.mfs.client.im.dao;

import com.mfs.client.im.exception.DaoException;

public interface BaseDao {

	public boolean save(Object obj) throws DaoException;

	public boolean update(Object obj) throws DaoException;

	public boolean saveOrUpdate(Object obj) throws DaoException;

	public boolean delete(Object obj) throws DaoException;
	
}
