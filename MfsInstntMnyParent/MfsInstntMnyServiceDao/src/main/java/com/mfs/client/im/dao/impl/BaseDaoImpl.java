package com.mfs.client.im.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.im.dao.BaseDao;
import com.mfs.client.im.dto.ResponseStatus;
import com.mfs.client.im.exception.DaoException;
import com.mfs.client.im.util.MFSIMCode;

public class BaseDaoImpl implements BaseDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(BaseDaoImpl.class);
	
	@Transactional
	public boolean save(Object obj) throws DaoException {
		LOGGER.debug("Inside BaseDAO Save");
		
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().save(obj);
			isSuccess = true;
		} 
	    catch (Exception e) {
			LOGGER.error("==>Exception thrown in BaseDaoImpl in save "+ e);
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSIMCode.ER204.getCode());
			status.setStatusMessage(MFSIMCode.ER204.getMessage());
			
			throw new DaoException(status);
		}
		return isSuccess;
	}

	@Transactional
	public boolean update(Object obj) throws DaoException {
		LOGGER.debug("Inside BaseDAO Update");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().update(obj);
			isSuccess = true;
		}catch (Exception e) {
			
			LOGGER.error("==>Exception thrown in BaseDaoImpl in update"+e);
			ResponseStatus status=new ResponseStatus();
			status.setStatusCode(MFSIMCode.ER205.getCode());
			status.setStatusMessage(MFSIMCode.ER205.getMessage());
			
			throw new DaoException(status);
		}
		return isSuccess;
	}

	@Transactional
	public boolean saveOrUpdate(Object obj) throws DaoException {
		LOGGER.debug("Inside BaseDAO saveOrUpdate");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().saveOrUpdate(obj);
			isSuccess = true;
		}catch (Exception e) {
			LOGGER.error("Exception in BaseDaoImpl in saveOrUpdate "+e);
			throw new DaoException("");
		}
		return isSuccess;
	}

	@Transactional
	public boolean delete(Object obj) throws DaoException {
		LOGGER.debug("Inside BaseDAO delete");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().delete(obj);
			isSuccess = true;
		} catch (Exception e) {
			LOGGER.error("Exception in BaseDaoImpl in delete "+e);
			throw new DaoException("");
		}
		return isSuccess;
	}

}