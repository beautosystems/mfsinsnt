package com.mfs.client.im.dao;

import com.mfs.client.im.dto.ValidateMobileRequestDto;
import com.mfs.client.im.exception.DaoException;
import com.mfs.client.im.model.ImKycDetails;
import com.mfs.client.im.model.ImSystemConfig;
import com.mfs.client.im.model.ImTransactionLog;

public interface TransactionDao extends BaseDao {

	public ImTransactionLog getTransactionByTransId(String mfsTransId) throws DaoException;

	public ImSystemConfig getSystemLog(String systemConfigId) throws DaoException;

	public Object getValidateMobile(ValidateMobileRequestDto request)throws DaoException;

	public ImKycDetails getUpdateValidateMobile(ValidateMobileRequestDto request) throws DaoException;

	public ImTransactionLog getTransactionByThirdPartyTransId(String thirdPartyTransId) throws DaoException;
}
