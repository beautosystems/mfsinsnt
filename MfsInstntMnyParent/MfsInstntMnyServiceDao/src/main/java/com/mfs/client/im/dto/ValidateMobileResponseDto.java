package com.mfs.client.im.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ValidateMobileResponseDto {
	
	private String code;

	private Response response;

	private String message;
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Response getReponse() {
		return response;
	}

	public void setReponse(Response reponse) {
		this.response = reponse;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

	@Override
	public String toString() {
		return "ValidateMobileResponseDto [code=" + code + ", reponse=" + response + ", message=" + message
				+ ", ]";
	}

	

	
	
	
	
	

}
