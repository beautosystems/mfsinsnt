package com.mfs.client.im.util;

public class CommonConstant {
	
	public static final String BASE_URL="base_url";
	public static final String AUTH_URL="auth_url";
	public static final String PAYOUT_URL = "payout_url";
	public static final String VALIDATE_URL = "validate_url";
	public static final String STATUS_TRANS_URL = "status_trans_url";
	public static final String FILE_PATH="file_path";
	
	public static final String CONTENT_TYPE = "Content-type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String AUTHORIZATION = "Authorization";
	public static final String TOKEN = "token";
	public static final String BEARER = "Bearer";
	public static final String multipart_formdata="multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW";
	public static final String Cache_Control="Cache-Control";
	public static final String no_cache="no-cache";
	
	public static final String GRANT_TYPE="grant_type";
	public static final String CLIENT_SECRET="client_secret"; ;
	public static final String CLIENT_ID="client_id";  
	public static final String USERNAME="username";  
	public static final String PASSWORD="password"; 
	public static final String ACCESS_TOKEN="access_token";
	public static final String REFRESH_TOKEN="refresh_token";
	
	public static final String INVALID_SENDER_FIRST_NAME = "Sender firstname is invalid";
	public static final String INVALID_SENDER_LAST_NAME = "Sender lastname is invalid";
	public static final String INVALID_SENDER_COUNTRY = "Sender country is invalid";
	public static final String INVALID_RECEIVER_FIRST_NAME = "Receiver firstname is invalid";
	public static final String INVALID_RECEIVER_LAST_NAME = "Receiver lastname is invalid";
	public static final String INVALID_RECEIVER_MSISDN= "Receiver mobile number is invalid";
	public static final String INVALID_RECEIVER_COUNTRY = "Receiver Country is invalid";
	public static final String INVALID_AMOUNT = "Sending amount is invalid";
	public static final String INVALID_RECEIVER_CURRENCY = "Receiver currency is invalid";
	public static final String INVALID_MNO = "Mobile Network Operator is invalid";
	public static final String INVALID_EXTR_ID = "Extr Id is invalid";
	public static final String INVALID_SERVICE_TYPE = "Service type is invalid";
	public static final String INVALID_ROUTING_NUMBER = "Routing number is invalid";
	public static final String INVALID_ACCOUNT_NUMBER = "Account number is invalid";
	public static final String INVALID_ID = "ID in request is invalid";
	public static final String SAVE_STATUS = "Initiated";
	public static final String SAVE_STATUS_MSG = "Transaction Initiated";
	public static final String UPDATE_STATUS = "Transaction Successfull";
	public static final String ERROR_STATUS = "Transaction Failed";
	
}
