package com.mfs.client.im.service;

import com.mfs.client.im.dto.TransactionDetailsResponse;

public interface GetTransactionService {

	public TransactionDetailsResponse getTransactionDetails(String id);
}
