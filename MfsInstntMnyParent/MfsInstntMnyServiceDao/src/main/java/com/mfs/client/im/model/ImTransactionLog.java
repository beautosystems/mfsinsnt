package com.mfs.client.im.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "im_transaction_log")
public class ImTransactionLog {

	@Id
	@GeneratedValue
	@Column(name = "transaction_id")
	private int transactionId;

	@Column(name = "sender_first_name")
	private String senderFirstName;

	@Column(name = "sender_last_name")
	private String senderLastName;

	@Column(name = "sender_country")
	private String senderCountry;

	@Column(name = "receiver_first_name")
	private String receiverFirstName;

	@Column(name = "receiver_last_name")
	private String receiverLastName;

	@Column(name = "receiver_msisdn")
	private String receiverMsisdn;

	@Column(name = "receiver_country")
	private String receiverCountry;

	@Column(name = "address")
	private String address;

	@Column(name = "receiver_currency")
	private String receiverCurrency;

	@Column(name = "mno")
	private String mno;

	@Column(name = "transaction_type")
	private String transactionType;

	@Column(name = "mfs_trans_id")
	private String mfsTransId;

	@Column(name = "service_type")
	private String serviceType;

	@Column(name = "routing_number")
	private String routingNumber;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "third_party_trans_id")
	private String thirdPartyTransId;

	@Column(name = "code")
	private String code;

	@Column(name = "message")
	private String message;

	@Column(name = "status")
	private String status;

	@Column(name = "amount_sent")
	private double amountSent;

	@Column(name = "amount_payout")
	private double amountPayout;

	@Column(name = "date_logged")
	private String dateLogged;

	@Column(name = "fees")
	private String fess;

	@Column(name = "timezone_type")
	private String timezoneType;

	@Column(name = "timezone")
	private String timezone;

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getSenderFirstName() {
		return senderFirstName;
	}

	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}

	public String getSenderLastName() {
		return senderLastName;
	}

	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}

	public String getSenderCountry() {
		return senderCountry;
	}

	public void setSenderCountry(String senderCountry) {
		this.senderCountry = senderCountry;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public String getReceiverMsisdn() {
		return receiverMsisdn;
	}

	public void setReceiverMsisdn(String receiverMsisdn) {
		this.receiverMsisdn = receiverMsisdn;
	}

	public String getReceiverCountry() {
		return receiverCountry;
	}

	public void setReceiverCountry(String receiverCountry) {
		this.receiverCountry = receiverCountry;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getReceiverCurrency() {
		return receiverCurrency;
	}

	public void setReceiverCurrency(String receiverCurrency) {
		this.receiverCurrency = receiverCurrency;
	}

	public String getMno() {
		return mno;
	}

	public void setMno(String mno) {
		this.mno = mno;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getMfsTransId() {
		return mfsTransId;
	}

	public void setMfsTransId(String mfsTransId) {
		this.mfsTransId = mfsTransId;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getThirdPartyTransId() {
		return thirdPartyTransId;
	}

	public void setThirdPartyTransId(String thirdPartyTransId) {
		this.thirdPartyTransId = thirdPartyTransId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getAmountSent() {
		return amountSent;
	}

	public void setAmountSent(double amountSent) {
		this.amountSent = amountSent;
	}

	public double getAmountPayout() {
		return amountPayout;
	}

	public void setAmountPayout(double amountPayout) {
		this.amountPayout = amountPayout;
	}

	public String getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(String dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getFess() {
		return fess;
	}

	public void setFess(String fess) {
		this.fess = fess;
	}

	public String getTimezoneType() {
		return timezoneType;
	}

	public void setTimezoneType(String timezoneType) {
		this.timezoneType = timezoneType;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	@Override
	public String toString() {
		return "ImTransactionLog [transactionId=" + transactionId + ", senderFirstName=" + senderFirstName
				+ ", senderLastName=" + senderLastName + ", senderCountry=" + senderCountry + ", receiverFirstName="
				+ receiverFirstName + ", receiverLastName=" + receiverLastName + ", receiverMsisdn=" + receiverMsisdn
				+ ", receiverCountry=" + receiverCountry + ", address=" + address + ", receiverCurrency="
				+ receiverCurrency + ", mno=" + mno + ", transactionType=" + transactionType + ", mfsTransId="
				+ mfsTransId + ", serviceType=" + serviceType + ", routingNumber=" + routingNumber + ", accountNumber="
				+ accountNumber + ", thirdPartyTransId=" + thirdPartyTransId + ", code=" + code + ", message=" + message
				+ ", status=" + status + ", amountSent=" + amountSent + ", amountPayout=" + amountPayout
				+ ", dateLogged=" + dateLogged + ", fess=" + fess + ", timezoneType=" + timezoneType + ", timezone="
				+ timezone + "]";
	}

}
