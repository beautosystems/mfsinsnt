package com.mfs.client.im.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder("doubleValue")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PayoutResponseDto {
	
	private String code;
	
	private String zeepay_id;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private double amount;
	
	private String message;
	
	private ResponseStatus responseStatus;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getZeepay_id() {
		return zeepay_id;
	}

	public void setZeepay_id(String zeepay_id) {
		this.zeepay_id = zeepay_id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	@Override
	public String toString() {
		return "PayoutResponseDto [code=" + code + ", zeepay_id=" + zeepay_id + ", amount=" + amount + ", message="
				+ message + ", responseStatus=" + responseStatus + "]";
	}

	
	
	
	
	

}
