package com.mfs.client.im.util;

public class CommonValidations {
	
	public boolean validateStringValues(String reqParamValue)
	{
		boolean validateResult = false;
		
		if(reqParamValue == null || reqParamValue.equals(" ") || reqParamValue.equals(""))
			validateResult = true;
		
		return validateResult;
	}
	
	public boolean validateAmountValues(Double amount)
	{
		boolean validateResult = false;
		if (amount == null) 
			validateResult = true;
		
		return validateResult;
	}

}
