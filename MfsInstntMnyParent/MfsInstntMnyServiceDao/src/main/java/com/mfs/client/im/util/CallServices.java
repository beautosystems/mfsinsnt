package com.mfs.client.im.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;

import com.mfs.client.util.HttpConnectorImpl;
import com.mfs.client.util.HttpsConnectionRequest;
import com.mfs.client.util.HttpsConnectionResponse;
import com.mfs.client.util.HttpsConnectorImpl;


public class CallServices {
	
private static final Logger LOGGER = Logger.getLogger(CallServices.class);
	
public static String getResponseFromService(String url, String request) throws Exception {
	
	String queryParam = ConvertJSONToQueryParam.getQueryParam(request);
	
	HttpsConnectionResponse httpsConResult = null;
	
	String str_result = null;
	
	LOGGER.info("Request Body =>");
	LOGGER.info(request);
	try {
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		
		Map<String, String> headerMap = new HashMap<String, String>();

		headerMap.put("Content-Type", "application/x-www-form-urlencoded");
		LOGGER.debug("URL : "+url);
		connectionRequest.setServiceUrl(url);
		connectionRequest.setHeaders(headerMap);
		connectionRequest.setHttpmethodName("POST");
		boolean isHttps = true;
		if (isHttps) {
			connectionRequest.setPort(8443);
			HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
			httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(queryParam, connectionRequest);
		} else {
			HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
			httpsConResult=httpConnectorImpl.httpUrlConnection(connectionRequest,queryParam);
		}
		str_result=httpsConResult.getResponseData();
		
		LOGGER.info("Response Body =>");
		LOGGER.info(str_result);
	} catch (Exception e) {			
		LOGGER.error(e);
		throw new Exception(e);
	}
	return str_result;
}
	
	
	public static String getTransactionResponseFromService(String url,String id,String accessToken) throws Exception {

	//	String queryParam = ConvertJSONToQueryParam.getQueryParam(id);
		HttpsConnectionResponse httpsConResult = null;
		String str_result = null;
		String serviceUrl=url+"/"+id;
		
		LOGGER.info("In Callservices in getTransactionResponseFromService Request Body =>");
		LOGGER.info("url= "+url +" "+"accessToken= "+ accessToken);
		try {
			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
			
			/*Map<String, String> headerMap = new HashMap<String, String>();

			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
			headerMap.put(CommonConstant.AUTHORIZATION, accessToken);*/
			
			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put("Authorization", "Bearer " + accessToken);
			headerMap.put("Content-Type", "application/x-www-form-urlencoded");
			LOGGER.info("Bearer " + accessToken);
			

			LOGGER.debug("URL : "+url);
			connectionRequest.setServiceUrl(serviceUrl);
			connectionRequest.setHeaders(headerMap);
			connectionRequest.setHttpmethodName("GET");
			
			
			
			HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
			httpsConResult=httpConnectorImpl.httpUrlConnections(connectionRequest);
			str_result=httpsConResult.getResponseData();
			
			LOGGER.info("In Callservices in getResponseFromService(url,accessToken) Response Body =>");
			LOGGER.info(str_result);
		} catch (Exception e) {			
			LOGGER.error("==>Exception thrown in CallServices in getResponseFromService(url,accessToken) : "+e);
			throw new Exception(e);
		}
		return str_result;
	}
	//payout and validationservice
	public static HttpResponse  getResponseFromServices(String url,String accessToken,String filePath) throws Exception {

		LOGGER.info("In Callservices in getResponseFromService Request Body =>");
		LOGGER.info("url= "+url +" "+"accessToken= "+ accessToken);

		HttpResponse str_result = null;

		try {
			HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		
			LOGGER.debug("URL : "+url);
			connectionRequest.setServiceUrl(url);
			connectionRequest.setHttpmethodName("POST");

			HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
			str_result=httpConnectorImpl.httpUrlConnections(connectionRequest,accessToken,filePath);

			LOGGER.info("In Callservices in getResponseFromService Response Body =>");
			LOGGER.info(str_result);

		} catch (Exception e) {	
			LOGGER.error("==>Exception thrown in CallServices in getResponseFromService : "+e);
			throw new Exception(e);
		}
		   return str_result;
		}
		
	//can use for validation payout transcationstatus methods
	public static String getResponseFromServices1(String url, String request,String accessToken) throws Exception {

			String queryParam = ConvertJSONToQueryParam.getQueryParam(request);
			
			HttpsConnectionResponse httpsConResult = null;
			
			String str_result = null;
			
			LOGGER.info("Request Body =>");
			LOGGER.info(queryParam);
			System.out.println(queryParam);
			
			try {
				HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
				
				Map<String, String> headerMap = new HashMap<String, String>();
				headerMap.put("Authorization", "Bearer " + accessToken);
				headerMap.put("Content-Type", "application/x-www-form-urlencoded");
				LOGGER.info("Bearer " + accessToken);
				
				LOGGER.info("URL : "+url);
				connectionRequest.setServiceUrl(url);
				connectionRequest.setHeaders(headerMap);
				connectionRequest.setHttpmethodName("POST");
				boolean isHttps = true;
				if (isHttps) {
					connectionRequest.setPort(8443);
					HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
					httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(queryParam, connectionRequest);
					
				} else {
					HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
					httpsConResult=httpConnectorImpl.httpUrlConnection(connectionRequest,queryParam);
				}
				str_result=httpsConResult.getResponseData();
				
				
				LOGGER.info("Response Body =>");
				LOGGER.info(str_result);
			} catch (Exception e) {			
				LOGGER.error(e);
				throw new Exception(e);
			}
			return str_result;
		}

}
