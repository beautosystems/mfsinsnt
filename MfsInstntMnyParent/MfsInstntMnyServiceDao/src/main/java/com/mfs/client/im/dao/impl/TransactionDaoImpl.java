package com.mfs.client.im.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.im.dao.TransactionDao;
import com.mfs.client.im.dto.ResponseStatus;
import com.mfs.client.im.dto.ValidateMobileRequestDto;
import com.mfs.client.im.exception.DaoException;
import com.mfs.client.im.model.ImKycDetails;
import com.mfs.client.im.model.ImSystemConfig;
import com.mfs.client.im.model.ImTransactionLog;
import com.mfs.client.im.util.MFSIMCode;

@Repository("TransactionDao")
@EnableTransactionManagement
public class TransactionDaoImpl extends BaseDaoImpl implements TransactionDao {

	@Autowired
	SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(TransactionDaoImpl.class);

	@Transactional
	public ImTransactionLog getTransactionByTransId(String mfsTransId) throws DaoException {
		LOGGER.debug("Inside getTransactionByTransId of TransactionDaoImpl request: " + mfsTransId);
		ImTransactionLog imTransactionLog = null;
		try {
			Session session = sessionFactory.getCurrentSession();
			String hql = " From ImTransactionLog where mfsTransId = :mfsTransId";
			Query query = session.createQuery(hql).setParameter("mfsTransId", mfsTransId);

			imTransactionLog = (ImTransactionLog) query.uniqueResult();

			
		} catch (Exception e) {
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSIMCode.ER210.getCode());
			status.setStatusMessage(MFSIMCode.ER210.getMessage());

			throw new DaoException(status);
		}

		LOGGER.debug("Inside getTransactionByTransId of TransactionDaoImpl db response: " + imTransactionLog);
		return imTransactionLog;

	}

	@Transactional
	public ImSystemConfig getSystemLog(String configKey) throws DaoException {

		LOGGER.debug("Inside getSystemLog of TransactionDaoImpl");
		ImSystemConfig imSystemConfig = null;
		try {
			Session session = sessionFactory.getCurrentSession();

			String hql = " From ImSystemConfig where configKey = :configKey";
			Query query = session.createQuery(hql);
			query.setString("configKey", configKey);
			imSystemConfig = (ImSystemConfig) query.uniqueResult();
		} catch (Exception e) {
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSIMCode.ER207.getCode());
			status.setStatusMessage(MFSIMCode.ER207.getMessage());

			throw new DaoException(status);
		}

		LOGGER.debug("Inside getSystemLog of TransactionDaoImpl db response:" + imSystemConfig);
		return imSystemConfig;
	}

	@Transactional
	public Object getValidateMobile(ValidateMobileRequestDto req) throws DaoException {
		LOGGER.debug("Inside getValidateMobile of TransactionDaoImpl request" + req);

		Query q = null;
		Object details = null;
		Session session = null;

		String walletHql = "select mobileNumber From ImKycDetails where mobileNumber = :mobileNumber";
		String bankhql = "select accountNumber From ImKycDetails where accountNumber = :accountNumber";

		try {
			session = sessionFactory.getCurrentSession();
			if (req.getService_type().equalsIgnoreCase("wallet")) {
				q = session.createQuery(walletHql);
				q.setParameter("mobileNumber", req.getMobile_number());
				details = q.uniqueResult();

			} else {
				q = session.createQuery(bankhql);
				q.setParameter("accountNumber", req.getAccount_number());
				details = q.uniqueResult();
			}
		} catch (Exception e) {
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSIMCode.ER208.getCode());
			status.setStatusMessage(MFSIMCode.ER208.getMessage());

			throw new DaoException(status);

		}

		LOGGER.debug("Inside getValidateMobile of TransactionDaoImpl db response" + details);
		return details;
	}

	@Transactional
	public ImKycDetails getUpdateValidateMobile(ValidateMobileRequestDto req) throws DaoException {
		LOGGER.debug("Inside getUpdateValidateMobile of TransactionDaoImpl request" + req);
		Query query;
		ImKycDetails imKycDetails = null;
		
		try {
			Session session = sessionFactory.getCurrentSession();
			
			

			if (req.getService_type().equalsIgnoreCase("wallet")) {
				String walletHql = " From ImKycDetails where mobileNumber = :mobileNumber";
				query = session.createQuery(walletHql).setParameter("mobileNumber", req.getMobile_number());
			} else {
				String bankHql = " From ImKycDetails where accountNumber = :accountNumber";
				query = session.createQuery(bankHql).setParameter("accountNumber", req.getAccount_number());
			}

			imKycDetails = (ImKycDetails) query.uniqueResult();

		} catch (Exception e) {
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSIMCode.ER209.getCode());
			status.setStatusMessage(MFSIMCode.ER209.getMessage());

			throw new DaoException(status);
		}
		LOGGER.debug("Inside getUpdateValidateMobile of TransactionDaoImpl db response" + imKycDetails);
		return imKycDetails;
	}

	@Transactional
	public ImTransactionLog getTransactionByThirdPartyTransId(String thirdPartyTransId) throws DaoException {
		LOGGER.debug("Inside getTransactionByThirdPartyTransId of TransactionDaoImpl request" + thirdPartyTransId);
		ImTransactionLog imTransactionLog = null;
		try {
			
			
			
			Session session = sessionFactory.getCurrentSession();
			String hql = " From ImTransactionLog where thirdPartyTransId = :thirdPartyTransId";
			Query query = session.createQuery(hql).setParameter("thirdPartyTransId", thirdPartyTransId);

			imTransactionLog = (ImTransactionLog) query.uniqueResult();

			
		} catch (Exception e) {
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(MFSIMCode.ER211.getCode());
			status.setStatusMessage(MFSIMCode.ER211.getMessage());

			throw new DaoException(status);
		}
		
		LOGGER.debug("Inside getTransactionByThirdPartyTransId of TransactionDaoImpl db response" + imTransactionLog);
		return imTransactionLog;

	}

}
