package com.mfs.client.im.dto;

public class ValidateMobileRequestDto {
	
	private String service_type;
	
	private String routing_number;
	
	private String account_number;
	
	private String mobile_number;
	
	private String mno;

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getRouting_number() {
		return routing_number;
	}

	public void setRouting_number(String routing_number) {
		this.routing_number = routing_number;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}

	public String getMno() {
		return mno;
	}

	public void setMno(String mno) {
		this.mno = mno;
	}

	@Override
	public String toString() {
		return "ValidateMobileRequestDto [service_type=" + service_type + ", routing_number=" + routing_number
				+ ", account_number=" + account_number + ", mobile_number=" + mobile_number + ", mno=" + mno + "]";
	}
	
	
	
	
	

}
