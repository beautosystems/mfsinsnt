package com.mfs.client.im.dto;

public class PayoutRequestDto {
	
	private String sender_first_name;
	
	private String sender_last_name;
	
	private String sender_country;
	
	private String receiver_first_name;
	
	private String receiver_last_name;
	
	private String receiver_msisdn;
	
	private String receiver_country;
	
	private double amount;
	
	private String address;
	
	private String receiver_currency;
	
	private String mno;
	
	private String transaction_type;
	
	private String extr_id;
	
	private String service_type;
	
	private String routing_number;
	
	private String account_number;

	public String getSender_first_name() {
		return sender_first_name;
	}

	public void setSender_first_name(String sender_first_name) {
		this.sender_first_name = sender_first_name;
	}

	public String getSender_last_name() {
		return sender_last_name;
	}

	public void setSender_last_name(String sender_last_name) {
		this.sender_last_name = sender_last_name;
	}

	public String getSender_country() {
		return sender_country;
	}

	public void setSender_country(String sender_country) {
		this.sender_country = sender_country;
	}

	public String getReceiver_first_name() {
		return receiver_first_name;
	}

	public void setReceiver_first_name(String receiver_first_name) {
		this.receiver_first_name = receiver_first_name;
	}

	public String getReceiver_last_name() {
		return receiver_last_name;
	}

	public void setReceiver_last_name(String receiver_last_name) {
		this.receiver_last_name = receiver_last_name;
	}

	public String getReceiver_msisdn() {
		return receiver_msisdn;
	}

	public void setReceiver_msisdn(String receiver_msisdn) {
		this.receiver_msisdn = receiver_msisdn;
	}

	public String getReceiver_country() {
		return receiver_country;
	}

	public void setReceiver_country(String receiver_country) {
		this.receiver_country = receiver_country;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getReceiver_currency() {
		return receiver_currency;
	}

	public void setReceiver_currency(String receiver_currency) {
		this.receiver_currency = receiver_currency;
	}

	public String getMno() {
		return mno;
	}

	public void setMno(String mno) {
		this.mno = mno;
	}

	public String getTransaction_type() {
		return transaction_type;
	}

	public void setTransaction_type(String transaction_type) {
		this.transaction_type = transaction_type;
	}

	public String getExtr_id() {
		return extr_id;
	}

	public void setExtr_id(String extr_id) {
		this.extr_id = extr_id;
	}

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getRouting_number() {
		return routing_number;
	}

	public void setRouting_number(String routing_number) {
		this.routing_number = routing_number;
	}

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

	@Override
	public String toString() {
		return "PayoutRequestDto [sender_first_name=" + sender_first_name + ", sender_last_name=" + sender_last_name
				+ ", sender_country=" + sender_country + ", receiver_first_name=" + receiver_first_name
				+ ", receiver_last_name=" + receiver_last_name + ", receiver_msisdn=" + receiver_msisdn
				+ ", receiver_country=" + receiver_country + ", amount=" + amount + ", address=" + address
				+ ", receiver_currency=" + receiver_currency + ", mno=" + mno + ", transaction_type=" + transaction_type
				+ ", extr_id=" + extr_id + ", service_type=" + service_type + ", routing_number=" + routing_number
				+ ", account_number=" + account_number + "]";
	}
	
	
	

}
