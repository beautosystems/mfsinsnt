package com.mfs.client.im.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonPropertyOrder("doubleValue")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data {
	
	private String zeepay_id;
	
	private String extr_id;
	
	private String sender_first_name;
	
	private String sender_last_name;
	
	private String sender_country;
	
	private String recipient_first_name;
	
	private String recipient_last_name;
	
	private String service_type;
	
	private String mobile_account;
	
	private String fess;
	
	private String status;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private double amount_sent;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	private double amount_payout;
	
	private CreatedAt created_at;
	
	
	
	private String message;
	
	private String code;
	

	public String getZeepay_id() {
		return zeepay_id;
	}

	public void setZeepay_id(String zeepay_id) {
		this.zeepay_id = zeepay_id;
	}

	public String getExtr_id() {
		return extr_id;
	}

	public void setExtr_id(String extr_id) {
		this.extr_id = extr_id;
	}

	public String getSender_first_name() {
		return sender_first_name;
	}

	public void setSender_first_name(String sender_first_name) {
		this.sender_first_name = sender_first_name;
	}

	public String getSender_last_name() {
		return sender_last_name;
	}

	public void setSender_last_name(String sender_last_name) {
		this.sender_last_name = sender_last_name;
	}

	public String getSender_country() {
		return sender_country;
	}

	public void setSender_country(String sender_country) {
		this.sender_country = sender_country;
	}

	public String getRecipient_first_name() {
		return recipient_first_name;
	}

	public void setRecipient_first_name(String recipient_first_name) {
		this.recipient_first_name = recipient_first_name;
	}

	public String getRecipient_last_name() {
		return recipient_last_name;
	}

	public void setRecipient_last_name(String recipient_last_name) {
		this.recipient_last_name = recipient_last_name;
	}

	public String getService_type() {
		return service_type;
	}

	public void setService_type(String service_type) {
		this.service_type = service_type;
	}

	public String getMobile_account() {
		return mobile_account;
	}

	public void setMobile_account(String mobile_account) {
		this.mobile_account = mobile_account;
	}

	public String getFess() {
		return fess;
	}

	public void setFess(String fess) {
		this.fess = fess;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getAmount_sent() {
		return amount_sent;
	}

	public void setAmount_sent(double amount_sent) {
		this.amount_sent = amount_sent;
	}

	public double getAmount_payout() {
		return amount_payout;
	}

	public void setAmount_payout(double amount_payout) {
		this.amount_payout = amount_payout;
	}

	public CreatedAt getCreated_at() {
		return created_at;
	}

	public void setCreated_at(CreatedAt created_at) {
		this.created_at = created_at;
	}

	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "TransactionDetailsResponseDto [zeepay_id=" + zeepay_id + ", extr_id=" + extr_id + ", sender_first_name="
				+ sender_first_name + ", sender_last_name=" + sender_last_name + ", sender_country=" + sender_country
				+ ", recipient_first_name=" + recipient_first_name + ", recipient_last_name=" + recipient_last_name
				+ ", service_type=" + service_type + ", mobile_account=" + mobile_account + ", fess=" + fess
				+ ", status=" + status + ", amount_sent=" + amount_sent + ", amount_payout=" + amount_payout
				+ ", created_at=" + created_at + ",  message=" + message
				+ ", code=" + code + "]";
	}

	

   
}
