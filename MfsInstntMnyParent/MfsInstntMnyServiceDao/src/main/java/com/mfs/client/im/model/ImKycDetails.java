package com.mfs.client.im.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "im_kyc_details")
public class ImKycDetails {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;

	@Column(name = "service_type")
	private String serviceType;

	@Column(name = "routing_number")
	private String routingNumber;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "mobile_number")
	private String mobileNumber;

	@Column(name = "mno")
	private String mno;

	@Column(name = "registration_status")
	private String registrationStatus;

	@Column(name = "registered_name")
	private String registeredName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getMno() {
		return mno;
	}

	public void setMno(String mno) {
		this.mno = mno;
	}

	public String getRegistrationStatus() {
		return registrationStatus;
	}

	public void setRegistrationStatus(String registrationStatus) {
		this.registrationStatus = registrationStatus;
	}

	public String getRegisteredName() {
		return registeredName;
	}

	public void setRegisteredName(String registeredName) {
		this.registeredName = registeredName;
	}

	@Override
	public String toString() {
		return "ImKycDetails [id=" + id + ", serviceType=" + serviceType + ", routingNumber=" + routingNumber
				+ ", accountNumber=" + accountNumber + ", mobileNumber=" + mobileNumber + ", mno=" + mno
				+ ", registrationStatus=" + registrationStatus + ", registeredName=" + registeredName + "]";
	}

}
