package com.mfs.client.im.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "im_trans_session")
public class ImTransSession {

	@Id
	@GeneratedValue
	@Column(name = "trans_session_id")
	private long transSessionId;

	@Column(name = "sender_first_name")
	private String senderFirstName;

	@Column(name = "sender_last_name")
	private String senderLastName;

	@Column(name = "receiver_first_name")
	private String receiverFirstName;

	@Column(name = "receiver_last_name")
	private String receiverLastName;

	@Column(name = "receiver_msisdn")
	private String receiverMsisdn;

	@Column(name = "mno")
	private String mno;

	@Column(name = "mfs_trans_id")
	private String mfsTransId;

	@Column(name = "service_type")
	private String serviceType;

	@Column(name = "routing_number")
	private String routingNumber;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "third_party_trans_id")
	private String thirdPartyTransId;

	@Column(name = "message")
	private String message;

	@Column(name = "status")
	private String status;

	@Column(name = "amount_sent")
	private double amountSent;

	@Column(name = "amount_payout")
	private double amountPayout;

	@Column(name = "date_logged")
	private Date dateLogged;

	public long getTransSessionId() {
		return transSessionId;
	}

	public void setTransSessionId(long transSessionId) {
		this.transSessionId = transSessionId;
	}

	public String getSenderFirstName() {
		return senderFirstName;
	}

	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}

	public String getSenderLastName() {
		return senderLastName;
	}

	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public String getReceiverMsisdn() {
		return receiverMsisdn;
	}

	public void setReceiverMsisdn(String receiverMsisdn) {
		this.receiverMsisdn = receiverMsisdn;
	}

	public String getMno() {
		return mno;
	}

	public void setMno(String mno) {
		this.mno = mno;
	}

	public String getMfsTransId() {
		return mfsTransId;
	}

	public void setMfsTransId(String mfsTransId) {
		this.mfsTransId = mfsTransId;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getThirdPartyTransId() {
		return thirdPartyTransId;
	}

	public void setThirdPartyTransId(String thirdPartyTransId) {
		this.thirdPartyTransId = thirdPartyTransId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	

	public double getAmountSent() {
		return amountSent;
	}

	public void setAmountSent(double amountSent) {
		this.amountSent = amountSent;
	}

	public double getAmountPayout() {
		return amountPayout;
	}

	public void setAmountPayout(double amountPayout) {
		this.amountPayout = amountPayout;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "ImTransSession [transSessionId=" + transSessionId + ", senderFirstName=" + senderFirstName
				+ ", senderLastName=" + senderLastName + ", receiverFirstName=" + receiverFirstName
				+ ", receiverLastName=" + receiverLastName + ", receiverMsisdn=" + receiverMsisdn + ", mno=" + mno
				+ ", mfsTransId=" + mfsTransId + ", serviceType=" + serviceType + ", routingNumber=" + routingNumber
				+ ", accountNumber=" + accountNumber + ", thirdPartyTransId=" + thirdPartyTransId + ", message="
				+ message + ", status=" + status + ", amountSent=" + amountSent + ", amountPayout=" + amountPayout
				+ ", dateLogged=" + dateLogged + "]";
	}


}
