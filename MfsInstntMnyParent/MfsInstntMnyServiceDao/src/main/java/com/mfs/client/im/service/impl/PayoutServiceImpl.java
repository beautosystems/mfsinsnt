package com.mfs.client.im.service.impl;

import java.util.Date;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.mfs.client.im.dao.SystemConfigDetailsDao;
import com.mfs.client.im.dao.TransactionDao;
import com.mfs.client.im.dto.PayoutRequestDto;
import com.mfs.client.im.dto.PayoutResponseDto;
import com.mfs.client.im.dto.ResponseStatus;
import com.mfs.client.im.exception.DaoException;
import com.mfs.client.im.model.ImTransactionError;
import com.mfs.client.im.model.ImTransactionLog;
import com.mfs.client.im.service.PayoutService;
import com.mfs.client.im.util.CallServices;
import com.mfs.client.im.util.CommonConstant;
import com.mfs.client.im.util.MFSIMCode;
import com.mfs.client.util.JSONToObjectConversion;

@Service("PayoutService")
public class PayoutServiceImpl extends BaseServiceImpl implements PayoutService {

	private static final Logger LOGGER = Logger.getLogger(PayoutServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDetailsDao systemconfigdao;

	public PayoutResponseDto payout(PayoutRequestDto request) {

		LOGGER.info("==>In PayoutServiceImpl in payout function payoutRequest =" + request);
		String serviceResponse = " ";
		Gson gson = new Gson();
		PayoutResponseDto response = null;
		ResponseStatus status = null;
		logTransSession(request.getSender_first_name(),request.getSender_last_name(),request.getReceiver_first_name(),request.getReceiver_last_name(),request.getReceiver_msisdn(),request.getMno(),request.getExtr_id(),request.getService_type(),request.getRouting_number(),request.getAccount_number()," ","Transaction Initiated","Init",request.getAmount(),0.0,new Date());
	
		
		if(request.getService_type().equalsIgnoreCase("bank")|| request.getService_type().equalsIgnoreCase("wallet") || request.getService_type().equalsIgnoreCase("pickup") )
		{
		
		try {
		Map<String, String> systemconfigdetails = systemconfigdao.getConfigDetailsMap();
		
		
		// Check MFS Transaction Id already processed
		ImTransactionLog imTransactionLog = (ImTransactionLog) transactionDao
				.getTransactionByTransId(request.getExtr_id());

		if (imTransactionLog != null) {

			response = new PayoutResponseDto();
			response.setCode(MFSIMCode.IMCODE_ERROR_TNX_DUPLICATE.getCode());
			response.setMessage(MFSIMCode.IMCODE_ERROR_TNX_DUPLICATE.getMessage());

		}

		else {
			
				logPayoutTransaction(request);

				LOGGER.info("==> Payout Request : " + gson.toJson(request));
				LOGGER.info("==> Sending Request to Url : " + systemconfigdetails.get(CommonConstant.BASE_URL)
						+ systemconfigdetails.get(CommonConstant.PAYOUT_URL));

				serviceResponse = CallServices.getResponseFromServices1(systemconfigdetails.get(CommonConstant.BASE_URL)
						+ systemconfigdetails.get(CommonConstant.PAYOUT_URL),
						gson.toJson(request), systemconfigdetails.get(CommonConstant.ACCESS_TOKEN));
		
	
			
			//	LOGGER.info("Response from HTTP Method : " + serviceResponse);
				LOGGER.info("In PayoutServiceImpl Response from HTTP Method : "+ serviceResponse);
				
				response = (PayoutResponseDto) JSONToObjectConversion.getObjectFromJson(serviceResponse,
						PayoutResponseDto.class);

			

				LOGGER.info("==>In PayoutServiceImpl Response from HTTP Method : "+ response);
				LOGGER.info("==>In PayoutServiceImpl String Response from HTTP method :"+ response.toString());

			
				updatePayoutTransaction(response, request.getExtr_id());

				if (response == null) {
					response = logPayoutErrorTransaction(request, MFSIMCode.ER203.getCode(),
							MFSIMCode.ER203.getMessage());
				}
				logTransSession(request.getSender_first_name(),request.getSender_last_name(),request.getReceiver_first_name(),request.getReceiver_last_name(),request.getReceiver_msisdn(),request.getMno(),request.getExtr_id(),request.getService_type(),request.getRouting_number(),request.getAccount_number(),response.getZeepay_id(),response.getMessage(),response.getCode(),request.getAmount(),response.getAmount(),new Date());
		}
			} catch (DaoException de) {
				LOGGER.error("==>DaoException in PayoutServiceImpl" + de);
				response = new PayoutResponseDto();
				status = new ResponseStatus();
				status.setStatusCode(de.getStatus().getStatusCode());
				status.setStatusMessage(de.getStatus().getStatusMessage());
				response.setResponseStatus(status);

			} catch (Exception e) {

				LOGGER.error("Exception in PayoutServiceImpl " + e);
				logPayoutErrorTransaction(request, MFSIMCode.IMCODE_ERROR_TIMEOUT.getCode(),
						MFSIMCode.IMCODE_ERROR_TIMEOUT.getMessage()); 
				response = new PayoutResponseDto();
				response.setMessage(MFSIMCode.IMCODE_ERROR_TIMEOUT.getMessage());
				response.setCode(MFSIMCode.IMCODE_ERROR_TIMEOUT.getCode());

			}
		}
		else
		{
			
			response = new PayoutResponseDto();
			response.setCode(MFSIMCode.VALIDATION_ERROR.getCode());
			response.setMessage(
					MFSIMCode.VALIDATION_ERROR.getMessage() + " " + CommonConstant.INVALID_SERVICE_TYPE);
		}
		
		return response;
	}

	public void logPayoutTransaction(PayoutRequestDto request) throws DaoException {
		ImTransactionLog logModel = new ImTransactionLog();

		logModel.setSenderFirstName(request.getSender_first_name());
		logModel.setSenderLastName(request.getSender_last_name());
		logModel.setSenderCountry(request.getSender_country());
		logModel.setReceiverFirstName(request.getReceiver_first_name());
		logModel.setReceiverLastName(request.getReceiver_last_name());
		logModel.setReceiverMsisdn(request.getReceiver_msisdn());
		logModel.setReceiverCountry(request.getReceiver_country());
		logModel.setAmountSent(request.getAmount());
		logModel.setAddress(request.getAddress());
		logModel.setReceiverCurrency(request.getReceiver_currency());
		logModel.setMno(request.getMno());
		logModel.setTransactionType(request.getTransaction_type());
		logModel.setMfsTransId(request.getExtr_id());
		logModel.setServiceType(request.getService_type());
		logModel.setRoutingNumber(request.getRouting_number());
		logModel.setAccountNumber(request.getAccount_number());
		logModel.setStatus(CommonConstant.SAVE_STATUS);
		logModel.setMessage(CommonConstant.SAVE_STATUS_MSG);

		transactionDao.save(logModel);

	
	}

	public void updatePayoutTransaction(PayoutResponseDto response, String mfsTransId) throws DaoException {
		ImTransactionLog logModel = (ImTransactionLog) transactionDao.getTransactionByTransId(mfsTransId);

		if (logModel != null) {

			if(response == null)
			{
				logModel.setStatus(CommonConstant.ERROR_STATUS);
				logModel.setCode(MFSIMCode.ER206.getCode());
				logModel.setMessage(MFSIMCode.ER206.getMessage());
				transactionDao.update(logModel);
			}
			
			logModel.setStatus(CommonConstant.UPDATE_STATUS);
			logModel.setCode(response.getCode());
			logModel.setMessage(response.getMessage());
			logModel.setThirdPartyTransId(response.getZeepay_id());
			logModel.setAmountSent(response.getAmount());
			transactionDao.update(logModel);
		}

	}

	public PayoutResponseDto logPayoutErrorTransaction(PayoutRequestDto request, String code, String msg) {
		ImTransactionError errModel = new ImTransactionError();
		PayoutResponseDto response = new PayoutResponseDto();
		errModel.setSenderFirstName(request.getSender_first_name());
		errModel.setSenderLastName(request.getSender_last_name());
		errModel.setSenderCountry(request.getSender_country());
		errModel.setReceiverFirstName(request.getReceiver_first_name());
		errModel.setReceiverLastName(request.getReceiver_last_name());
		errModel.setReceiverMsisdn(request.getReceiver_msisdn());
		errModel.setReceiverCountry(request.getReceiver_country());
		errModel.setAmountSent(request.getAmount());
		errModel.setMno(request.getMno());
		errModel.setMfsTransId(request.getExtr_id());
		errModel.setServiceType(request.getService_type());
		errModel.setRoutingNumber(request.getRouting_number());
		errModel.setAccountNumber(request.getAccount_number());
		errModel.setErrorCode(code);
		errModel.setErrorMessage(msg);

		try {
			transactionDao.save(errModel);
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in PayoutServiceImpl" + de);

			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(de.getStatus().getStatusCode());
			status.setStatusMessage(de.getStatus().getStatusMessage());
			response.setResponseStatus(status);
		}
		return response;
	}

}