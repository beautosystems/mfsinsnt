package com.mfs.client.im.util;

public enum MFSIMCode {
	
	IMCODE103("103","Wallet Balance Ceiling Breach"),
	IMCODE403("403","Invalid/Non-existant wallet"),
	IMCODE419("419","Sanctions/OFAC list breach"),
	IMCODE402("402","Client payment required"),
	IMCODE200("200","Validated Successfully"),
	
	VALIDATION_ERROR("ER201","Validation Error"),
	IMCODE_ERROR_TIMEOUT("ER202","Connection Timeout"),
	ER203("ER203","Exception while processing your request, Please try again later."),
	ER204("ER204","Exception while save data"),
	ER205("ER205","Exception while update"),
	ER206("ER206","Null Response"),
	ER207("ER207","Exception while getting config details"),
	ER208("ER208","Exception while getting account details"),
	ER209("ER209","Exception while updating account details "),
	ER210("ER210","Exception while getting transaction by mfsTransId "),
	ER211("ER211","Exception while getting transaction by thirdPartyTransId "),
	ER212("ER212","get Kyc Details Failed "),
	ER213("ER213","Exception while processing your response, Please try again later."),
	IMCODE_ERROR_TNX_DUPLICATE("ER212","Duplicate Transaction.");
	
	
	
    private String code;
	private String message;

	private MFSIMCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}


}
