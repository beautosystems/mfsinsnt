package com.mfs.client.im.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.im.dao.SystemConfigDetailsDao;
import com.mfs.client.im.dto.ResponseStatus;
import com.mfs.client.im.exception.DaoException;
import com.mfs.client.im.model.ImSystemConfig;
import com.mfs.client.im.util.MFSIMCode;

@EnableTransactionManagement
@Repository("SystemConfigDetailsDao")
public class SystemConfigDetailsDaoImpl implements SystemConfigDetailsDao {

	@Autowired
	private SessionFactory sessionfactory;
	
	private static final Logger LOGGER = Logger.getLogger(SystemConfigDetailsDaoImpl.class);
	
	@Transactional
	public Map<String, String> getConfigDetailsMap()throws DaoException {
		LOGGER.info("Inside  getConfigDetailsMap");
		
		Map<String, String> imSystemConfigurationMap = null;
		
		Session session = sessionfactory.getCurrentSession();
		String hql = "From ImSystemConfig";
		Query query = session.createQuery(hql);
		try
		{
			List<ImSystemConfig> imSystemConfigurationList = query.list();
		
			if (imSystemConfigurationList != null && !imSystemConfigurationList.isEmpty()) {
				imSystemConfigurationMap = new HashMap<String, String>();
				for (ImSystemConfig imSystemConfiguration : imSystemConfigurationList) {
					imSystemConfigurationMap.put(imSystemConfiguration.getConfigKey(),
					imSystemConfiguration.getConfigValue());
				}
			}
		}
		catch (Exception e) {
			LOGGER.error("==>Exception thrown in getConfigDetailsMap ");
			ResponseStatus status= new ResponseStatus();
			status.setStatusCode(MFSIMCode.ER207.getCode());
			status.setStatusMessage(MFSIMCode.ER207.getMessage());
			throw new DaoException(status);
		}
		return imSystemConfigurationMap;
	}

}
