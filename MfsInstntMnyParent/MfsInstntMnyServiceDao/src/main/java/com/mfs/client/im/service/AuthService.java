package com.mfs.client.im.service;

import com.mfs.client.im.dto.AuthResponseDto;

public interface AuthService {

	public AuthResponseDto getAccessToken();
	
}
