package com.mfs.client.im.util;

public class ConvertJSONToQueryParam {
	public static String getQueryParam(String json_input) {
		json_input=json_input.replace(":","=");
		json_input=json_input.replace(",","&");
		json_input=json_input.replace("\"","");
		json_input=json_input.replace("{","");
		json_input=json_input.replace("}","");
		json_input=json_input.replace("\\n","");
		json_input=json_input.replace(" ","");
		
		return json_input;
	}
}
