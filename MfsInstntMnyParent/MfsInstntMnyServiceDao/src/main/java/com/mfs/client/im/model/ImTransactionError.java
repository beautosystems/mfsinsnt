package com.mfs.client.im.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "im_transaction_error")
public class ImTransactionError {

	@Id
	@GeneratedValue
	@Column(name = "transaction_error_id")
	private int transactionErrorId;

	@Column(name = "sender_first_name")
	private String senderFirstName;

	@Column(name = "sender_last_name")
	private String senderLastName;

	@Column(name = "sender_country")
	private String senderCountry;

	@Column(name = "receiver_first_name")
	private String receiverFirstName;

	@Column(name = "receiver_last_name")
	private String receiverLastName;

	@Column(name = "receiver_msisdn")
	private String receiverMsisdn;

	@Column(name = "receiver_country")
	private String receiverCountry;

	@Column(name = "amount_sent")
	private double amountSent;

	@Column(name = "mno")
	private String mno;

	@Column(name = "mfs_trans_id")
	private String mfsTransId;

	@Column(name = "service_type")
	private String serviceType;

	@Column(name = "routing_number")
	private String routingNumber;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "error_code")
	private String errorCode;

	@Column(name = "error_message")
	private String errorMessage;

	@Column(name = "mobile_account")
	private String mobileAccount;

	@Column(name = "amount_payout")
	private double amountPayout;

	@Column(name = "date_logged")
	private String dateLogged;

	public int getTransactionErrorId() {
		return transactionErrorId;
	}

	public void setTransactionErrorId(int transactionErrorId) {
		this.transactionErrorId = transactionErrorId;
	}

	public String getSenderFirstName() {
		return senderFirstName;
	}

	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}

	public String getSenderLastName() {
		return senderLastName;
	}

	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}

	public String getSenderCountry() {
		return senderCountry;
	}

	public void setSenderCountry(String senderCountry) {
		this.senderCountry = senderCountry;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public String getReceiverMsisdn() {
		return receiverMsisdn;
	}

	public void setReceiverMsisdn(String receiverMsisdn) {
		this.receiverMsisdn = receiverMsisdn;
	}

	public String getReceiverCountry() {
		return receiverCountry;
	}

	public void setReceiverCountry(String receiverCountry) {
		this.receiverCountry = receiverCountry;
	}

	public double getAmountSent() {
		return amountSent;
	}

	public void setAmountSent(double amountSent) {
		this.amountSent = amountSent;
	}

	public String getMno() {
		return mno;
	}

	public void setMno(String mno) {
		this.mno = mno;
	}

	public String getMfsTransId() {
		return mfsTransId;
	}

	public void setMfsTransId(String mfsTransId) {
		this.mfsTransId = mfsTransId;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getMobileAccount() {
		return mobileAccount;
	}

	public void setMobileAccount(String mobileAccount) {
		this.mobileAccount = mobileAccount;
	}

	public double getAmountPayout() {
		return amountPayout;
	}

	public void setAmountPayout(double amountPayout) {
		this.amountPayout = amountPayout;
	}

	public String getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(String dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "ImtransactionError [transactionErrorId=" + transactionErrorId + ", senderFirstName=" + senderFirstName
				+ ", senderLastName=" + senderLastName + ", senderCountry=" + senderCountry + ", receiverFirstName="
				+ receiverFirstName + ", receiverLastName=" + receiverLastName + ", receiverMsisdn=" + receiverMsisdn
				+ ", receiverCountry=" + receiverCountry + ", amountSent=" + amountSent + ", mno=" + mno
				+ ", mfsTransId=" + mfsTransId + ", serviceType=" + serviceType + ", routingNumber=" + routingNumber
				+ ", accountNumber=" + accountNumber + ", errorCode=" + errorCode + ", errorMessage=" + errorMessage
				+ ", mobileAccount=" + mobileAccount + ", amountPayout=" + amountPayout + ", dateLogged=" + dateLogged
				+ "]";
	}

}
