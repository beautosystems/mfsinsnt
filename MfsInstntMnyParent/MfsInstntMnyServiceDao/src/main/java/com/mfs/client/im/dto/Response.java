package com.mfs.client.im.dto;

public class Response {
	
	private String registration_status;
	
	private String registered_name;

	public String getRegistration_status() {
		return registration_status;
	}

	public void setRegistration_status(String registration_status) {
		this.registration_status = registration_status;
	}

	public String getRegistered_name() {
		return registered_name;
	}

	public void setRegistered_name(String registered_name) {
		this.registered_name = registered_name;
	}

	@Override
	public String toString() {
		return "Response [registration_status=" + registration_status + ", registered_name=" + registered_name + "]";
	}

	
	
	

}
