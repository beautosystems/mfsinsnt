package com.mfs.client.im.service.impl;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.im.dao.SystemConfigDetailsDao;
import com.mfs.client.im.dao.TransactionDao;
import com.mfs.client.im.dto.AuthRequestDto;
import com.mfs.client.im.dto.AuthResponseDto;
import com.mfs.client.im.dto.ResponseStatus;
import com.mfs.client.im.exception.DaoException;
import com.mfs.client.im.model.ImSystemConfig;
import com.mfs.client.im.service.AuthService;
import com.mfs.client.im.util.CallServices;
import com.mfs.client.im.util.CommonConstant;
import com.mfs.client.im.util.MFSIMCode;
import com.mfs.client.util.JSONToObjectConversion;

@Service("AuthService")
public class AuthServiceImpl extends BaseServiceImpl implements AuthService {

	@Autowired
	TransactionDao transactioDao;

	@Autowired
	SystemConfigDetailsDao systemconfigdao;

	private static final Logger LOGGER = Logger.getLogger(AuthServiceImpl.class);

	public AuthResponseDto getAccessToken() {
		LOGGER.debug("Inside AuthServiceImpl");

		AuthResponseDto authResponse = new AuthResponseDto();
		AuthRequestDto request = new AuthRequestDto();
		Gson gson = new Gson();

		ResponseStatus status = null;
		String jsonResponse;

		try {
		Map<String, String> systemconfigdetails = systemconfigdao.getConfigDetailsMap();

		request.setClient_id(systemconfigdetails.get(CommonConstant.CLIENT_ID));
		request.setClient_secret(systemconfigdetails.get(CommonConstant.CLIENT_SECRET));
		request.setGrant_type(systemconfigdetails.get(CommonConstant.GRANT_TYPE));
		request.setPassword(systemconfigdetails.get(CommonConstant.PASSWORD));
		request.setUsername(systemconfigdetails.get(CommonConstant.USERNAME));

		LOGGER.info("In AuthServiceImpl authRequest details from db : " + request);
		
			LOGGER.info("In AuthServiceImpl authRequest url : " + systemconfigdetails.get(CommonConstant.BASE_URL)
					+ systemconfigdetails.get(CommonConstant.AUTH_URL));
			jsonResponse = CallServices.getResponseFromService(
					systemconfigdetails.get(CommonConstant.BASE_URL) + systemconfigdetails.get(CommonConstant.AUTH_URL),
					gson.toJson(request));
			
			LOGGER.info("In AuthServiceImpl Response from HTTP Method : " + jsonResponse);

			authResponse = (AuthResponseDto) JSONToObjectConversion.getObjectFromJson(jsonResponse,
					AuthResponseDto.class);
			LOGGER.info("In AuthServiceImpl String Response from HTTP method :" + authResponse.toString());

			if (authResponse != null) {
				ImSystemConfig imSystemConfig = transactioDao.getSystemLog(CommonConstant.ACCESS_TOKEN);
				imSystemConfig.setConfigValue(authResponse.getAccess_token());
				transactioDao.update(imSystemConfig);

				ImSystemConfig imSystemConfigg = transactioDao.getSystemLog(CommonConstant.REFRESH_TOKEN);
				imSystemConfigg.setConfigValue(authResponse.getRefresh_token());
				transactioDao.update(imSystemConfigg);
			}

			LOGGER.debug("Get Response From IM --> authServiceimpl" + authResponse);
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in AuthServiceImpl" + de);
			authResponse = new AuthResponseDto();
			status = new ResponseStatus();
			status.setStatusCode(de.getStatus().getStatusCode());
			status.setStatusMessage(de.getStatus().getStatusMessage());
			authResponse.setResponseStatus(status);

		} catch (Exception e) {
			LOGGER.error("==>Exception in AuthServiceImpl " + e);
			authResponse = new AuthResponseDto();
			authResponse.setMessage(MFSIMCode.ER203.getMessage());
			authResponse.setCode(MFSIMCode.ER203.getCode());

		}
		return authResponse;
	}

}