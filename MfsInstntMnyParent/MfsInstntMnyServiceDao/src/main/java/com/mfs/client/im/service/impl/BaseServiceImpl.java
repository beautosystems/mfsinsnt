package com.mfs.client.im.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.mfs.client.im.dao.LogTransSessionDao;
import com.mfs.client.im.model.ImTransSession;
import com.mfs.client.im.service.BaseService;


public class BaseServiceImpl implements BaseService {
	
	@Autowired
	LogTransSessionDao logTransSessionDao;

	public void logTransSession(String senderFirstName, String senderLastName, String receiverFirstName, String receiverLastName,
			String receiverMsisdn, String mno, String mfsTransId, String serviceType, String routingNumber,
			String accountNumber, String thirdPartyTransId, String message, String status, double amountSent,
			double amountPayout, Date dateLogged) {
		ImTransSession imTransSession=new ImTransSession();
		imTransSession.setAccountNumber(accountNumber);
		imTransSession.setAmountPayout(amountPayout);
		imTransSession.setAmountSent(amountSent);
		imTransSession.setDateLogged(dateLogged);
		imTransSession.setMessage(message);
		imTransSession.setMfsTransId(mfsTransId);
		imTransSession.setMno(mno);
		imTransSession.setReceiverFirstName(receiverFirstName);
		imTransSession.setReceiverLastName(receiverLastName);
		imTransSession.setReceiverMsisdn(receiverMsisdn);
		imTransSession.setRoutingNumber(routingNumber);
		imTransSession.setSenderFirstName(senderFirstName);
		imTransSession.setSenderLastName(senderLastName);
		imTransSession.setServiceType(serviceType);
		imTransSession.setStatus(status);
		imTransSession.setThirdPartyTransId(thirdPartyTransId);
		logTransSessionDao.logTransSession(imTransSession);
		
	}

}
