package com.mfs.client.im.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.im.dao.LogTransSessionDao;
import com.mfs.client.im.model.ImTransSession;

@Repository("LogTransSessionDao")
@EnableTransactionManagement
public class LogTransSessionDaoImpl implements LogTransSessionDao {
	@Autowired
	private SessionFactory sessionFactory;
	private static final Logger LOGGER = Logger
			.getLogger(LogTransSessionDaoImpl.class);

	@Transactional
	public long logTransSession(ImTransSession imTransSession) {

		long sessionId = 0;

		LOGGER.debug("Inside logTransSession of LogTransSessionDaoImpl");
		Session session = sessionFactory.getCurrentSession();
		sessionId = (Long) session.save(imTransSession);
		LOGGER.info("Trans Session Save " + imTransSession);
		return sessionId;

	}

}
