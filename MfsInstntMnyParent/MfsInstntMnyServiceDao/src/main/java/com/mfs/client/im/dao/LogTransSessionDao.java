package com.mfs.client.im.dao;

import com.mfs.client.im.model.ImTransSession;

public interface LogTransSessionDao {
	public long logTransSession(ImTransSession imTransSession);
}
