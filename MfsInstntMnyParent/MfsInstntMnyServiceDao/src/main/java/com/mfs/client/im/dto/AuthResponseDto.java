package com.mfs.client.im.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthResponseDto {
	
	private String token_type;
	
	private String expires_in;
	
	private String access_token;
	
	private String refresh_token;
	
	private String code;
	
	private String message;
	
	private ResponseStatus responseStatus;
	
	

	public ResponseStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(ResponseStatus responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getToken_type() {
		return token_type;
	}

	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	public String getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(String expires_in) {
		this.expires_in = expires_in;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "AuthResponseDto [token_type=" + token_type + ", expires_in=" + expires_in + ", access_token="
				+ access_token + ", refresh_token=" + refresh_token + ", code=" + code + ", message=" + message
				+ ", responseStatus=" + responseStatus + "]";
	}

	/*@Override
	public String toString() {
		return "AuthResponseDto [token_type=" + token_type + ", expires_in=" + expires_in + ", access_token="
				+ access_token + ", refresh_token=" + refresh_token + ", code=" + code + ", message=" + message + "]";
	}
*/
	

	
	
	

}
