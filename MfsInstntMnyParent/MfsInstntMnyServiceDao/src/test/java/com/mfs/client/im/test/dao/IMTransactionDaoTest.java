package com.mfs.client.im.test.dao;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.im.dao.TransactionDao;
import com.mfs.client.im.dto.ValidateMobileRequestDto;
import com.mfs.client.im.exception.DaoException;
import com.mfs.client.im.model.ImKycDetails;
import com.mfs.client.im.model.ImSystemConfig;
import com.mfs.client.im.model.ImTransactionLog;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class IMTransactionDaoTest {

	@Autowired
	TransactionDao transactionDao;

	private static final Logger LOGGER = Logger.getLogger(IMTransactionDaoTest.class);

	@Test
	@Ignore
	public void getTransactionByTransId() {
		ImTransactionLog imTransactionLog = new ImTransactionLog();
		try {
			imTransactionLog = (ImTransactionLog) transactionDao.getTransactionByTransId("Txn108");
		} catch (DaoException e) {
			LOGGER.error(e);
		}

		Assert.assertNotNull(imTransactionLog);
	}

	@Test
	@Ignore
	public void getSystemLog() {
		ImSystemConfig imSystemConfig = new ImSystemConfig();
		try {
			imSystemConfig = transactionDao.getSystemLog("assess_token");
		} catch (DaoException e) {
			LOGGER.error(e);
		}

		Assert.assertNotNull(imSystemConfig);
	}

	@Test
	@Ignore
	public void getValidateMobile() {
		ImKycDetails imKycDetails = new ImKycDetails();
		ValidateMobileRequestDto request = new ValidateMobileRequestDto();
		request.setService_type("bank");
		request.setAccount_number("03876");
		// request.setMno("idea");
		// request.setMobile_number("3467657");
		request.setRouting_number("02456");
		try {
			imKycDetails = (ImKycDetails) transactionDao.getValidateMobile(request);
		} catch (DaoException e) {
			LOGGER.error(e);
		}
		Assert.assertNotNull(imKycDetails);
	}

	@Test
	@Ignore
	public void getUpdateValidateMobile() {
		ImKycDetails imKycDetails = new ImKycDetails();
		ValidateMobileRequestDto res = new ValidateMobileRequestDto();

		res.setService_type("bank");
		res.setRouting_number("02456");
		res.setAccount_number("03876");
		// res.setMno("idea");
		// res.setMobile_number("3467657");
		try {
			imKycDetails = (ImKycDetails) transactionDao.getUpdateValidateMobile(res);
		} catch (DaoException e) {
			LOGGER.error(e);
		}
		Assert.assertNotNull(imKycDetails);

	}

	@Test
	@Ignore
	public void getTransactionByThirdPartyTransId() {
		ImTransactionLog imTransactionLog = new ImTransactionLog();
		try {
			imTransactionLog = (ImTransactionLog) transactionDao.getTransactionByThirdPartyTransId("DTrxn101");
		} catch (DaoException e) {
			LOGGER.error(e);
		}
		Assert.assertNotNull(imTransactionLog);
	}
}
