package com.mfs.client.im.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.im.dto.AuthResponseDto;
import com.mfs.client.im.service.AuthService;



@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AuthServiceTest {
	
	@Autowired
	AuthService authservice;
	
	//@Ignore
	@Test
	public void authKayTest()
	{
		
		AuthResponseDto authresponse=authservice.getAccessToken();
		
		//Assert.assertNotNull(authresponse);
		System.out.println("response"+authresponse.toString());
	}

}
