package com.mfs.client.im.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.im.dto.ValidateMobileRequestDto;
import com.mfs.client.im.dto.ValidateMobileResponseDto;
import com.mfs.client.im.service.ValidateService1;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class ValidateServiceTest {
	
	@Autowired
	ValidateService1 validateservice1;
	
	//@Ignore
	@Test
	public void ValidateMobileTest()
	{
		ValidateMobileRequestDto validaterequest=new ValidateMobileRequestDto();
		validaterequest.setService_type("Wallet");
		validaterequest.setRouting_number("");
		validaterequest.setAccount_number("");
		validaterequest.setMobile_number("233270369883");
		validaterequest.setMno("TIGO");
		
		
		ValidateMobileResponseDto validateMobileResponseDto = validateservice1.validate(validaterequest);
		//Assert.assertNotNull(validaterequest);
		System.out.println(validateMobileResponseDto.toString());
	}
}

