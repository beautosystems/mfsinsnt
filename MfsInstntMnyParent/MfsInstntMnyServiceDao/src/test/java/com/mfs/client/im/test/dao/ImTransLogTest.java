package com.mfs.client.im.test.dao;

import java.util.Date;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.im.dao.LogTransSessionDao;
import com.mfs.client.im.model.ImTransSession;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class ImTransLogTest {
	
	@Autowired
	LogTransSessionDao logTransSessionDao;
	
	@Test
	@Ignore
	public void logTransSession()
	{
		ImTransSession imTransSession=new ImTransSession();
		imTransSession.setAccountNumber("9089743249");
		imTransSession.setAmountPayout(200);
		imTransSession.setAmountSent(599);
		imTransSession.setDateLogged(new Date());
		imTransSession.setMessage("Success");
		imTransSession.setMfsTransId("Mfs11");
		imTransSession.setMno("Airtel");
		imTransSession.setReceiverFirstName("Amit");
		imTransSession.setReceiverLastName("Yadav");
		imTransSession.setReceiverMsisdn("9989898383");
		imTransSession.setRoutingNumber("98");
		imTransSession.setSenderFirstName("Beauto");
		imTransSession.setSenderLastName("System");
		imTransSession.setServiceType("TransSession");
		imTransSession.setThirdPartyTransId("7327593275");
		long result = logTransSessionDao.logTransSession(imTransSession);
		Assert.assertEquals(1, result);
		
		
	}

}
