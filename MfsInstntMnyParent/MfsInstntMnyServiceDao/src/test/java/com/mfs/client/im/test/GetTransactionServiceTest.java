package com.mfs.client.im.test;


import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.im.dto.AuthResponseDto;
import com.mfs.client.im.dto.Data;
import com.mfs.client.im.dto.TransactionDetailsResponse;
import com.mfs.client.im.service.AuthService;
import com.mfs.client.im.service.GetTransactionService;



@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetTransactionServiceTest{
	
	@Autowired
	GetTransactionService gettransservice;
	
	@Ignore
	@Test
	public void authKayTest()
	{
		
		
		TransactionDetailsResponse response=gettransservice.getTransactionDetails("Txn108");
		
		Assert.assertNotNull(response);
	}

}

