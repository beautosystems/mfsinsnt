package com.mfs.client.im.test;


import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.im.dto.PayoutRequestDto;
import com.mfs.client.im.dto.PayoutResponseDto;
import com.mfs.client.im.service.PayoutService;





@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class PayoutServiceTest {
	
	@Autowired
	PayoutService payoutservice;
	
	@Ignore
	@Test
	public void payoutTest()
	{
		PayoutRequestDto payoutrequest= new PayoutRequestDto();
		
		payoutrequest.setSender_first_name("Pallavi1");
		payoutrequest.setSender_last_name("Mhetre1");
		payoutrequest.setReceiver_country("india");
		payoutrequest.setReceiver_first_name("Shubham1");
		payoutrequest.setReceiver_last_name("Bhople1");
		payoutrequest.setReceiver_msisdn("7654321235");
		payoutrequest.setAmount(550.0);
		payoutrequest.setAddress("pune");
		payoutrequest.setReceiver_currency("Rupee");
		payoutrequest.setMno("Airtel");
		payoutrequest.setTransaction_type("Credit");
		payoutrequest.setService_type("wallet");
		payoutrequest.setExtr_id("Txn109");
		
			
		PayoutResponseDto response = payoutservice.payout(payoutrequest);
		
		Assert.assertNotNull(response);
	}

}

